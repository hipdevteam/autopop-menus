# Autopopulate Menus
Autopopulate WordPress submenu items based on taxonomy, post type, or post hierarchy.

## Current Limitations
The autopopulated submenus are currently limited to a single level. It won't populate submenus of submenus, yet.

## Installation
Currently, the only supported method of installation is with composer. From the root of your project, type:  
`composer require pucklabs/autopop-menus`

If you aren't familiar with how to use composer with Wordpress, [ check out this thorough guide ](https://roots.io/using-composer-with-wordpress/) 
